import { Component } from '@angular/core';

@Component({
  selector: 'app-studiengaenge', // Selector für die Komponente
  templateUrl: './studiengaenge.component.html', // Pfad zur HTML-Datei der Komponente
  styleUrl: './studiengaenge.component.scss' // Pfad zur SCSS-Datei der Komponente
})
export class StudiengaengeComponent {
  // Arrays für Texte, Bilder und Titel der Karten
  postTexts = [
    'Digital Business' ,
    'Professional Software Engineering Digital Business Management Digital Business Engineering',
    'Hochschule Reutlingen - Standort Böblingen'
  ];

  postImages = [
    'assets/img/Bilder/bachelor.jpg',
    'assets/img/Bilder/master.jpg',
    'assets/img/Bilder/hhz.jpg'
  ];
  
  postTitel = [
    'Bachelorstudiengang',
    'Masterstudiengang',
    'Das Herman Hollerith Zentrum'
  ];
}
