import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudiengaengeComponent } from './studiengaenge.component';

describe('StudiengaengeComponent', () => {
  let component: StudiengaengeComponent;
  let fixture: ComponentFixture<StudiengaengeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StudiengaengeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudiengaengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
