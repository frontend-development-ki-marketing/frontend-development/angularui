import { Component } from '@angular/core';

@Component({
  selector: 'app-board', // Definiert den Selektor für diese Komponente
  templateUrl: './board.component.html', // Pfad zur HTML-Template-Datei
  styleUrls: ['./board.component.scss'] // Pfad zur SCSS-Stylesheet-Datei
})
export class BoardComponent {
  squares: string[] = Array(9).fill(null); // Array, das den Zustand der 9 Spielfelder speichert
  xIsNext: boolean = true; // Boolean, der anzeigt, ob 'X' als nächstes dran ist
  winner: string | null = null; // Variable, die den Gewinner speichert oder null, wenn es keinen gibt

  get player() {
    return this.xIsNext ? 'X' : 'O'; // Gibt 'X' zurück, wenn xIsNext true ist, sonst 'O'
  }

  makeMove(idx: number) {
    if (!this.squares[idx] && !this.winner) { // Wenn das Feld leer ist und es keinen Gewinner gibt
      this.squares.splice(idx, 1, this.player); // Setzt das aktuelle Feld auf den Spieler 'X' oder 'O'
      this.xIsNext = !this.xIsNext; // Wechselt den Spieler
      this.calculateWinner(); // Überprüft, ob es einen Gewinner gibt
    }
  }

  calculateWinner() {
    const winningLines = [ // Alle möglichen Gewinnkombinationen
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    for (let line of winningLines) { // Überprüft jede Gewinnkombination
      const [a, b, c] = line;
      if (this.squares[a] && this.squares[a] === this.squares[b] && this.squares[a] === this.squares[c]) {
        this.winner = this.squares[a]; // Setzt den Gewinner, wenn eine Gewinnkombination gefunden wird
        return;
      }
    }

    if (!this.squares.includes(null)) { // Wenn es keine leeren Felder mehr gibt und kein Gewinner
      this.winner = 'Draw'; // Setzt den Gewinner auf 'Draw' (Unentschieden)
    }
  }

  resetGame() {
    this.squares = Array(9).fill(null); // Setzt alle Spielfelder zurück
    this.winner = null; // Setzt den Gewinner zurück
    this.xIsNext = true; // Setzt den Startspieler auf 'X'
  }
}
