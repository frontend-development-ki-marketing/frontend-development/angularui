import { Component } from '@angular/core';

@Component({
  selector: 'app-alle',
  templateUrl: './alle.component.html',
  styleUrl: './alle.component.scss'
})
export class AlleComponent {
  // Ein Array von Texten für die Beschreibungen der Karten
  postTexts = [
    'Lernen Sie die Studiengänge näher kennen und finden Sie für sich heraus, welcher am besten zu Ihnen passt!',
    'Lassen Sie ihre Emotionen von einer KI erkennen!',
    'Spielen Sie Tic Tac Toe gegen eine KI und gewinnen Sie Preise!'
  ];

  // Ein Array von Bildpfaden für die Karten
  postImages = [
    'assets/img/Bilder/quiz.jpg',
    'assets/img/Bilder/emotionen.jpg',
    'assets/img/Bilder/tictactoe.jpg'
  ]
  
  // Ein Array von Titeln für die Karten
  postTitel = [
    'Quiz',
    'Gesichtserkennung',
    'TicTacToe'
  ]
}
