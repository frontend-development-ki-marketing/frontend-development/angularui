import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlleComponent } from './alle.component';

describe('AlleComponent', () => {
  let component: AlleComponent;
  let fixture: ComponentFixture<AlleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AlleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AlleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
