import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-square', // Der Selektor, der das Komponentenelement identifiziert
  templateUrl: './square.component.html', // Die Datei, die das HTML-Template der Komponente enthält
  styleUrls: ['./square.component.scss'] // Die Datei(en), die die CSS-Stile der Komponente enthalten
})
export class SquareComponent {
  @Input() value: 'X' | 'O' | null; // Eingabeparameter zur Darstellung des Werts einer Zelle

  constructor() {
    this.value = null; // Initialisiert den Wert der Zelle als null
  }
}
