import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GesichtserkennungComponent } from './gesichtserkennung.component';

describe('GesichtserkennungComponent', () => {
  let component: GesichtserkennungComponent;
  let fixture: ComponentFixture<GesichtserkennungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GesichtserkennungComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(GesichtserkennungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
