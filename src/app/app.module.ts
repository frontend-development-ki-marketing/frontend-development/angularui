import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CardsComponent } from './cards/cards.component';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';



import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { NavigationComponent } from './navigation/navigation.component';
import { StandortComponent } from './standort/standort.component';
import { AlleComponent } from './alle/alle.component';
import { GesichtserkennungComponent } from './gesichtserkennung/gesichtserkennung.component';
import { GamesComponent } from './games/games.component';
import { StudiengaengeComponent } from './studiengaenge/studiengaenge.component';
import { BachelorComponent } from './bachelor/bachelor.component';
import { MasterComponent } from './master/master.component';
import { HomeComponent } from './home/home.component';
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';
import { FooterComponent } from './footer/footer.component';
import { MaskottchenComponent } from './maskottchen/maskottchen.component';
import { ChatPageComponent } from './chat-page/chat-page.component';








@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CardsComponent,
    NavigationComponent,
    StandortComponent,
    AlleComponent,
    GesichtserkennungComponent,
    GamesComponent,
    StudiengaengeComponent,
    BachelorComponent,
    MasterComponent,
    HomeComponent,
    BoardComponent,
    SquareComponent,
    FooterComponent,
    MaskottchenComponent,
    ChatPageComponent,
   
    
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule
  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
