import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
    // Abonnement auf Router-Ereignisse, um die Sichtbarkeit des Zurück-Buttons zu aktualisieren
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Prüfe, ob die aktuelle Seite die Home-Seite ist
        // und aktualisiere die Sichtbarkeit des Zurück-Buttons
        this.showBackButton();
      }
    });
  }

  // Methode zum Navigieren zur Home-Seite
  goBack() {
    this.router.navigate(['/home']);
  }

  // Methode zur Bestimmung der Sichtbarkeit des Zurück-Buttons
  showBackButton(): boolean {
    // Überprüfe, ob die aktuelle Route nicht die Home-Seite ist
    const currentRoute = this.router.url;
    return currentRoute !== '/home';
  }
}
