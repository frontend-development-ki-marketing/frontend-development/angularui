import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards', // Definiert den Selektor für diese Komponente
  templateUrl: './cards.component.html', // Pfad zur HTML-Template-Datei
  styleUrl: './cards.component.scss' // Pfad zur SCSS-Stylesheet-Datei
})
export class CardsComponent {

  @Input() text: string = ""; // Eingabeparameter für den Textinhalt der Karte
  @Input() img: string = ""; // Eingabeparameter für den Bildpfad der Karte
  @Input() titel: string = ""; // Eingabeparameter für den Titel der Karte

  constructor(private router: Router) {} // Injektion des Routers zum Navigieren

  handleCardClick(titel: string, img: string, text: string) {
    // Informationen über die geklickte Karte verwenden, um zur entsprechenden Seite zu navigieren
    console.log("Clicked card:", titel, img, text);
    
    // Navigieren zur entsprechenden Seite basierend auf den Karteninformationen
    if (titel === 'Quiz') {
      this.router.navigate(['/quiz'], { queryParams: { titel, img, text }});
    } else if (titel === 'Gesichtserkennung') {
      this.router.navigate(['/gesichtserkennung'], { queryParams: { titel, img, text }});
    } else if (titel === 'TicTacToe') {
      this.router.navigate(['/games'], { queryParams: { titel, img, text }});
    } else if (titel === 'Masterstudiengang') {
      this.router.navigate(['/master'], { queryParams: { titel, img, text }});
    } else if (titel === 'Bachelorstudiengang') {
      this.router.navigate(['/bachelor'], { queryParams: { titel, img, text }});
    } else if (titel === 'Das Herman Hollerith Zentrum') {
      this.router.navigate(['/standort'], { queryParams: { titel, img, text }});
    } else {
      // Fallback für unbekannte Karten
      console.error("Unknown card clicked:", titel);
    }
  }
}
