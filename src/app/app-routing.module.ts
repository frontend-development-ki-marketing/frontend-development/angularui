import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StandortComponent } from './standort/standort.component';
import { AppComponent } from './app.component';
import { AlleComponent } from './alle/alle.component';
import { GesichtserkennungComponent } from './gesichtserkennung/gesichtserkennung.component';
import { GamesComponent } from './games/games.component';
import { StudiengaengeComponent } from './studiengaenge/studiengaenge.component';
import { BachelorComponent } from './bachelor/bachelor.component';
import { MasterComponent } from './master/master.component';
import { HomeComponent } from './home/home.component';
import { ChatPageComponent } from './chat-page/chat-page.component';

// Definierte Routen für die Anwendung
const routes: Routes = [
  { path: 'standort', component: StandortComponent},
  { path: 'alle', component: AlleComponent},
  { path: 'gesichtserkennung', component: GesichtserkennungComponent},
  { path: 'games', component: GamesComponent},
  { path: 'studiengaenge', component: StudiengaengeComponent},
  { path: 'bachelor', component: BachelorComponent},
  { path: 'master', component: MasterComponent},
  { path: 'home', component: HomeComponent},
  { path: 'chat', component: ChatPageComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // Umleiten von der Standard-Startseite auf /home
  { path: '**', redirectTo: '/home' } // Umleiten von unbekannten Pfaden auf /home
  
];
  


@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule { }
