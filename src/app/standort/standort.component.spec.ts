import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StandortComponent } from './standort.component';

describe('StandortComponent', () => {
  let component: StandortComponent;
  let fixture: ComponentFixture<StandortComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StandortComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StandortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
