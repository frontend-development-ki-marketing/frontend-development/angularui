import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaskottchenComponent } from './maskottchen.component';

describe('MaskottchenComponent', () => {
  let component: MaskottchenComponent;
  let fixture: ComponentFixture<MaskottchenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MaskottchenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MaskottchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
